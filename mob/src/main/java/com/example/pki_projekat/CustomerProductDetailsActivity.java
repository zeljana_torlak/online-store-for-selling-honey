package com.example.pki_projekat;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pki_projekat.data.Basket;
import com.example.pki_projekat.data.Product;
import com.example.pki_projekat.data.User;

public class CustomerProductDetailsActivity extends AppCompatActivity {

    private User user;
    private Product product;

    private int quantity = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_product_details);

        ImageView imageView = (ImageView) findViewById(R.id.menuButton);
        imageView.setOnClickListener(v -> openContextMenu(v));
        registerForContextMenu(imageView);

        Intent intent = getIntent();
        int id = intent.getIntExtra("userId", 0);
        user = MainActivity.users.get(id);

        ((TextView) findViewById(R.id.menuLogo1)).setOnClickListener(v -> {
            startActivity((new Intent(this, CustomerActivity.class)).putExtra("userId", user.getId()));
        });
        ((TextView) findViewById(R.id.menuLogo2)).setOnClickListener(v -> {
            startActivity((new Intent(this, CustomerActivity.class)).putExtra("userId", user.getId()));
        });

        int productId = intent.getIntExtra("productId", 0);
        product = MainActivity.products.get(productId);

        ((TextView) findViewById(R.id.productDetailsName)).setText(product.getName());
        ((TextView) findViewById(R.id.productDetailsPrice)).setText(product.getPrice() + " din");
        int imageResource = getResources().getIdentifier("@drawable/product" + (product.getId() + 1), null, getPackageName());
        ((ImageView) findViewById(R.id.productDetailsImage)).setImageDrawable(getResources().getDrawable(imageResource));
        ((TextView) findViewById(R.id.productDetailsDescription)).setText(product.getDescription());
        ((TextView) findViewById(R.id.productDetailsWeight)).setText("Jedna tegla sadrži " + product.getWeight() + " grama meda.");
        ((TextView) findViewById(R.id.productDetailsUsage)).setText(product.getUsage());

        ((EditText) findViewById(R.id.addToBasketQuantity)).setText("1");
        ((Button) findViewById(R.id.decrementQuantity)).setOnClickListener(v -> {
            quantity = 1;
            if (!((EditText) findViewById(R.id.addToBasketQuantity)).getText().toString().isEmpty()){
                quantity = Integer.parseInt(((EditText) findViewById(R.id.addToBasketQuantity)).getText().toString());
                if (quantity > 1) quantity--;
                else quantity = 1;
            }
            ((EditText) findViewById(R.id.addToBasketQuantity)).setText(quantity + "");
        });
        ((Button) findViewById(R.id.incrementQuantity)).setOnClickListener(v -> {
            quantity = 1;
            if (!((EditText) findViewById(R.id.addToBasketQuantity)).getText().toString().isEmpty()){
                quantity = Integer.parseInt(((EditText) findViewById(R.id.addToBasketQuantity)).getText().toString());
                if (quantity > 0) quantity++;
                else quantity = 1;
            }
            ((EditText) findViewById(R.id.addToBasketQuantity)).setText(quantity + "");
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.customermenu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.kupacProfil:
                intent = new Intent(this, CustomerProfileActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacProizvodi:
                intent = new Intent(this, CustomerProductsActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacKorpa:
                intent = new Intent(this, CustomerBasketActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacOdjava:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }

    public void addToBasket(View view) {
        String quantityTemp = ((EditText) findViewById(R.id.addToBasketQuantity)).getText().toString();
        if (quantityTemp.isEmpty()){
            Toast.makeText(this, "Nevalidna količina.", Toast.LENGTH_SHORT).show();
            return;
        }

        quantity = Integer.parseInt(quantityTemp);
        if (quantity < 1){
            Toast.makeText(this, "Nevalidna količina.", Toast.LENGTH_SHORT).show();
            return;
        }

        Basket basket = null;
        for (Basket b: MainActivity.baskets) {
            if (b.getUserId() == user.getId()){
                basket = b;
                break;
            }
        }

        basket.addItem(product.getId(), quantity);
        Toast.makeText(this, "Artikal je uspešno dodat u korpu.", Toast.LENGTH_SHORT).show();
    }
}