package com.example.pki_projekat.data;

import java.util.LinkedList;

public class Basket {
    private int userId;
    private LinkedList<Integer> itemsProductId = new LinkedList<>();
    private LinkedList<Integer> itemsQuantity = new LinkedList<>();

    public Basket(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public LinkedList<Integer> getItemsProductId() {
        return itemsProductId;
    }

    public LinkedList<Integer> getItemsQuantity() {
        return itemsQuantity;
    }

    public void addItem(int productId, int quantity){
        int index = itemsProductId.indexOf(productId);
        if (index == -1){
            itemsProductId.add(productId);
            itemsQuantity.add(quantity);
        } else {
            itemsQuantity.set(index, itemsQuantity.get(index) + quantity);
        }
    }

    public int decrementQuantity(int productId){
        int index = itemsProductId.indexOf(productId);
        int quantityTemp = itemsQuantity.get(index);
        if (quantityTemp == 1){
            itemsProductId.remove(index);
            itemsQuantity.remove(index);
        } else {
            itemsQuantity.set(index, quantityTemp - 1);
        }
        return quantityTemp - 1;
    }

    public int incrementQuantity(int productId){
        int index = itemsProductId.indexOf(productId);
        int quantityTemp = itemsQuantity.get(index);
        itemsQuantity.set(index, quantityTemp + 1);
        return quantityTemp + 1;
    }

    public void removeAllItems(){
        itemsProductId.clear();
        itemsQuantity.clear();
    }
}
