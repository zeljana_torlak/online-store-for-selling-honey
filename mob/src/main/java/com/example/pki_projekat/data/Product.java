package com.example.pki_projekat.data;

public class Product {
    private int id;
    private String name;
    private int price;
    private String description;
    private int weight;
    private String usage;


    public Product(int id, String name, int price, String description, int weight, String usage) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.weight = weight;
        this.usage = usage;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public int getWeight() {
        return weight;
    }

    public String getUsage(){
        return usage;
    }
}
