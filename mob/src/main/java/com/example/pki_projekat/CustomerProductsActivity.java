package com.example.pki_projekat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pki_projekat.data.Product;
import com.example.pki_projekat.data.User;

public class CustomerProductsActivity extends AppCompatActivity {

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_products);

        ImageView imageView = (ImageView) findViewById(R.id.menuButton);
        imageView.setOnClickListener(v -> openContextMenu(v));
        registerForContextMenu(imageView);

        Intent intent = getIntent();
        int id = intent.getIntExtra("userId", 0);
        user = MainActivity.users.get(id);

        ((TextView) findViewById(R.id.menuLogo1)).setOnClickListener(v -> {
            startActivity((new Intent(this, CustomerActivity.class)).putExtra("userId", user.getId()));
        });
        ((TextView) findViewById(R.id.menuLogo2)).setOnClickListener(v -> {
            startActivity((new Intent(this, CustomerActivity.class)).putExtra("userId", user.getId()));
        });

        LinearLayout linearLayout1 = findViewById(R.id.productsLayout1);
        LinearLayout linearLayout2 = findViewById(R.id.productsLayout2);

        for (Product p: MainActivity.products){
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View myView = inflater.inflate(R.layout.product_layout, null);
            int imageResource = getResources().getIdentifier("@drawable/product" + (p.getId() + 1), null, getPackageName());
            ((ImageView) myView.findViewById(R.id.productImage)).setImageDrawable(getResources().getDrawable(imageResource));
            ((ImageView) myView.findViewById(R.id.productImage)).setOnClickListener(v -> {
                startActivity((new Intent(this, CustomerProductDetailsActivity.class)).putExtra("userId", user.getId()).putExtra("productId", p.getId()));
            });
            ((TextView) myView.findViewById(R.id.productName)).setText(p.getName());
            ((TextView) myView.findViewById(R.id.productPrice)).setText(p.getPrice() + " din");
            if (p.getId() % 2 == 0) {
                linearLayout1.addView(myView);
            } else{
                linearLayout2.addView(myView);
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.customermenu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.kupacProfil:
                intent = new Intent(this, CustomerProfileActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacProizvodi:
                intent = new Intent(this, CustomerProductsActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacKorpa:
                intent = new Intent(this, CustomerBasketActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacOdjava:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }
}