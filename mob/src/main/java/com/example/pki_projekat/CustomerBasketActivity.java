package com.example.pki_projekat;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pki_projekat.data.Basket;
import com.example.pki_projekat.data.Product;
import com.example.pki_projekat.data.User;

public class CustomerBasketActivity extends AppCompatActivity {

    private User user;
    private Basket basket;
    private long priceSum = 0;

    private int quantityTemp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_basket);

        ImageView imageView = (ImageView) findViewById(R.id.menuButton);
        imageView.setOnClickListener(v -> openContextMenu(v));
        registerForContextMenu(imageView);

        Intent intent = getIntent();
        int id = intent.getIntExtra("userId", 0);
        user = MainActivity.users.get(id);

        for (Basket b: MainActivity.baskets) {
            if (b.getUserId() == user.getId()){
                basket = b;
                break;
            }
        }

        ((TextView) findViewById(R.id.menuLogo1)).setOnClickListener(v -> {
            startActivity((new Intent(this, CustomerActivity.class)).putExtra("userId", user.getId()));
        });
        ((TextView) findViewById(R.id.menuLogo2)).setOnClickListener(v -> {
            startActivity((new Intent(this, CustomerActivity.class)).putExtra("userId", user.getId()));
        });

        if (basket.getItemsProductId().isEmpty()){
            ((TextView) findViewById(R.id.customerBasketTitle)).setText("Vaša korpa je trenutno prazna");
        } else {
            LinearLayout linearLayout = findViewById(R.id.basketItemsLayout);

            ((TextView) findViewById(R.id.basketProductName)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.basketProductQuantity)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.basketProductPriceSum)).setVisibility(View.VISIBLE);

            for (int i = 0; i < basket.getItemsProductId().size(); i++){
                LayoutInflater lineInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                linearLayout.addView(lineInflater.inflate(R.layout.horizontal_line_layout, null));

                Product p = MainActivity.products.get(basket.getItemsProductId().get(i));

                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View myView = inflater.inflate(R.layout.basket_item_layout, null);
                int imageResource = getResources().getIdentifier("@drawable/product" + (p.getId() + 1), null, getPackageName());
                ((ImageView) myView.findViewById(R.id.basketProductImage)).setImageDrawable(getResources().getDrawable(imageResource));
                ((ImageView) myView.findViewById(R.id.basketProductImage)).setOnClickListener(v -> {
                    startActivity((new Intent(this, CustomerProductDetailsActivity.class)).putExtra("userId", user.getId()).putExtra("productId", p.getId()));
                });
                ((TextView) myView.findViewById(R.id.basketProductName)).setText(p.getName() + ", " + p.getPrice() + " din");
                int itemPrice = basket.getItemsQuantity().get(i);
                ((TextView) myView.findViewById(R.id.basketQuantity)).setText(itemPrice + "");
                itemPrice *= p.getPrice();
                ((TextView) myView.findViewById(R.id.basketProductPriceSum)).setText(itemPrice + " din");

                ((Button) myView.findViewById(R.id.decrementBasketQuantity)).setOnClickListener(v -> {
                    quantityTemp = basket.decrementQuantity(p.getId());
                    if (quantityTemp == 0){
                        startActivity(new Intent(this, CustomerBasketActivity.class).putExtra("userId", user.getId()));
                        return;
                    }
                    ((TextView) myView.findViewById(R.id.basketQuantity)).setText(quantityTemp + "");
                    quantityTemp *= p.getPrice();
                    ((TextView) myView.findViewById(R.id.basketProductPriceSum)).setText(quantityTemp + " din");
                    priceSum -= p.getPrice();
                    ((TextView) findViewById(R.id.basketPriceSum)).setText(priceSum + " din");
                });
                ((Button) myView.findViewById(R.id.incrementBasketQuantity)).setOnClickListener(v -> {
                    quantityTemp = basket.incrementQuantity(p.getId());
                    ((TextView) myView.findViewById(R.id.basketQuantity)).setText(quantityTemp + "");
                    quantityTemp *= p.getPrice();
                    ((TextView) myView.findViewById(R.id.basketProductPriceSum)).setText(quantityTemp + " din");
                    priceSum += p.getPrice();
                    ((TextView) findViewById(R.id.basketPriceSum)).setText(priceSum + " din");
                });

                linearLayout.addView(myView);
                priceSum += itemPrice;
            }

            LayoutInflater lineInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            linearLayout.addView(lineInflater.inflate(R.layout.horizontal_line_layout, null));

            ((TextView) findViewById(R.id.basketPriceSumLabel)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.basketPriceSum)).setText(priceSum + " din");
            ((Button) findViewById(R.id.basketOrderButton)).setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.customermenu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.kupacProfil:
                intent = new Intent(this, CustomerProfileActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacProizvodi:
                intent = new Intent(this, CustomerProductsActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacKorpa:
                intent = new Intent(this, CustomerBasketActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacOdjava:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }

    public void order(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Da li ste sigurni da želite da izvršite narudžbinu?").setTitle("Pitanje");

        builder.setNegativeButton("Odustani", (dialog, which) -> {
            dialog.cancel();
        }).setPositiveButton("Potvrdi", (dialog, which) -> {
            basket.removeAllItems();
            Toast.makeText(getApplicationContext(), "Narudžbina je uspešno izvršena.", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, CustomerBasketActivity.class).putExtra("userId", user.getId()));
        });
        builder.create();
        builder.show();
    }
}