package com.example.pki_projekat;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pki_projekat.data.User;

public class CustomerChangeProfileActivity extends AppCompatActivity {

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_change_profile);

        ImageView imageView = (ImageView) findViewById(R.id.menuButton);
        imageView.setOnClickListener(v -> openContextMenu(v));
        registerForContextMenu(imageView);

        Intent intent = getIntent();
        int id = intent.getIntExtra("userId", 0);
        user = MainActivity.users.get(id);

        ((TextView) findViewById(R.id.menuLogo1)).setOnClickListener(v -> {
            startActivity((new Intent(this, CustomerActivity.class)).putExtra("userId", user.getId()));
        });
        ((TextView) findViewById(R.id.menuLogo2)).setOnClickListener(v -> {
            startActivity((new Intent(this, CustomerActivity.class)).putExtra("userId", user.getId()));
        });

        ((EditText) findViewById(R.id.customerChangeName)).setText(user.getName());
        ((EditText) findViewById(R.id.customerChangeSurname)).setText(user.getSurname());
        ((EditText) findViewById(R.id.customerChangePhoneNumber)).setText(user.getPhoneNumber());
        ((EditText) findViewById(R.id.customerChangeAddress)).setText(user.getAddress());
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.customermenu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.kupacProfil:
                intent = new Intent(this, CustomerProfileActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacProizvodi:
                intent = new Intent(this, CustomerProductsActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacKorpa:
                intent = new Intent(this, CustomerBasketActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacOdjava:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }

    public void goToProfile(View view) {
        Intent intent = new Intent(this, CustomerProfileActivity.class);
        intent.putExtra("userId", user.getId());
        startActivity(intent);
    }

    public void changeProfile(View view) {
        String name = ((EditText) findViewById(R.id.customerChangeName)).getText().toString();
        String surname = ((EditText) findViewById(R.id.customerChangeSurname)).getText().toString();
        String phoneNumber = ((EditText) findViewById(R.id.customerChangePhoneNumber)).getText().toString();
        String address = ((EditText) findViewById(R.id.customerChangeAddress)).getText().toString();

        if (name.isEmpty() || surname.isEmpty() || phoneNumber.isEmpty() || address.isEmpty()){
            Toast.makeText(this, "Morate uneti neophodne podatke.", Toast.LENGTH_SHORT).show();
            return;
        }

        //Toast.makeText(this, "Nevalidan broj telefona.", Toast.LENGTH_SHORT).show();

        user.setName(name);
        user.setSurname(surname);
        user.setPhoneNumber(phoneNumber);
        user.setAddress(address);
        Toast.makeText(this, "Lični podaci su uspešno promenjeni.", Toast.LENGTH_SHORT).show();
    }
}