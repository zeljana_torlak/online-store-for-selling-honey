package com.example.pki_projekat;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pki_projekat.data.User;

public class CustomerChangePasswordActivity extends AppCompatActivity {

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_change_password);

        ImageView imageView = (ImageView) findViewById(R.id.menuButton);
        imageView.setOnClickListener(v -> openContextMenu(v));
        registerForContextMenu(imageView);

        Intent intent = getIntent();
        int id = intent.getIntExtra("userId", 0);
        user = MainActivity.users.get(id);

        ((TextView) findViewById(R.id.menuLogo1)).setOnClickListener(v -> {
            startActivity((new Intent(this, CustomerActivity.class)).putExtra("userId", user.getId()));
        });
        ((TextView) findViewById(R.id.menuLogo2)).setOnClickListener(v -> {
            startActivity((new Intent(this, CustomerActivity.class)).putExtra("userId", user.getId()));
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.customermenu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.kupacProfil:
                intent = new Intent(this, CustomerProfileActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacProizvodi:
                intent = new Intent(this, CustomerProductsActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacKorpa:
                intent = new Intent(this, CustomerBasketActivity.class);
                intent.putExtra("userId", user.getId());
                startActivity(intent);
                return true;
            case R.id.kupacOdjava:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }

    public void goToProfile(View view) {
        Intent intent = new Intent(this, CustomerProfileActivity.class);
        intent.putExtra("userId", user.getId());
        startActivity(intent);
    }

    public void changePassword(View view) {
        String oldPassword = ((EditText) findViewById(R.id.customerChangeOldPassword)).getText().toString();
        String newPassword = ((EditText) findViewById(R.id.customerChangeNewPassword)).getText().toString();
        String confirmPassword = ((EditText) findViewById(R.id.customerChangeConfirmPassword)).getText().toString();

        boolean isOK = true;
        if (oldPassword.isEmpty() || newPassword.isEmpty() || confirmPassword.isEmpty()){
            Toast.makeText(this, "Morate uneti neophodne podatke.", Toast.LENGTH_SHORT).show();
            isOK = false;
        }
        if (!oldPassword.equals(user.getPassword())){
            Toast.makeText(this, "Loše ste uneli staru lozinku.", Toast.LENGTH_SHORT).show();
            isOK = false;
        }
        if (!newPassword.equals(confirmPassword)){
            Toast.makeText(this, "Unete lozinke se ne poklapaju.", Toast.LENGTH_SHORT).show();
            isOK = false;
        }
        if (!isOK){
            return;
        }

        user.setPassword(newPassword);
        Toast.makeText(this, "Lozinka je uspešno promenjena.", Toast.LENGTH_SHORT).show();
    }
}