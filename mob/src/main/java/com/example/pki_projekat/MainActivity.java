package com.example.pki_projekat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pki_projekat.data.Basket;
import com.example.pki_projekat.data.Product;
import com.example.pki_projekat.data.User;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    public static LinkedList<User> users = new LinkedList<>();
    public static LinkedList<Product> products = new LinkedList<>();
    public static LinkedList<Basket> baskets = new LinkedList<>();

    static {
        users.add(new User(0, "marko", "123", "Marko", "Marković", "0645676543", "Njegoševa 15", false));
        users.add(new User(1, "jelena", "123", "Jelena", "Jelić", "0643434567", "Krunska 5", true));
        users.add(new User(2, "ivana", "123", "Ivana", "Ivanović", "0645453521", "Vojislava Ilića 24", false));

        products.add(new Product(0, "Tegla meda, kupina", 1300,
                "Med od kupine odlikuje jedinstven voćni ukus i on predstavlja odličan zaslađivač, ali i sjajan izbor za sve namene.",
                454, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(1, "Tegla meda, detelina", 1100,
                "Med od deteline ima klasičan, zaokružen i poznat ukus koji je podložan širokom spektru kulinarskih namena.",
                454, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(2, "Tegla meda, malina", 1300,
                "Med od maline dolazi sa jedne male porodične farme. Svetle je boje, a ima blag i nežan ukus.",
                454, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(3, "Tegla meda, pomorandžin cvet", 1300,
                "Med od pomorandžinog cveta je svetle boje i laganog ukusa sa akcentom na citrus koji odražava delikatnost ovih cvetova.",
                454, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(4, "Tegla meda, divlje cveće", 1300,
                "Ovaj med ima taman, topao i bogat ukus koji se menja sa godišnjim dobom. Omiljeni je med profesionalnih pekara.",
                454, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(5, "Tegla meda, prolećni nektar", 1100,
                "Prolećni nektar je med koji podseća na blago začinjene prolećne cvetove i predstavlja sjajan dodatak jelima.",
                454, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(6, "Tegla meda, heljda", 1100,
                "Heljdin med je izrazito tamne boje i ima pun, bogat ukus. Tamniji medovi obično imaju veći sadržaj minerala nego svetliji.",
                454, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(7, "Tegla meda, čičak", 1300,
                "Ovaj med je lagane boje i ukusa, ali i odiše svežinom. Koristite ga kao zaslađivač u omiljenom receptu.",
                454, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(8, "Tegla meda, livadska pena", 1100,
                "Ovaj med ima lagan ukus koji podseća na vanilu. Veoma je popularan kod lokalnih ljubitelja meda.",
                454, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(9, "Tegla meda, borovnica", 1300,
                "Med  od borovnice ima lagan ukus sa suptilnim notama prepečenog belog sleza i vanile čineći ukusan dodatak jelima.",
                454, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(10, "Tegla meda, nana", 1300,
                "Med od nane odlikuje zlatna boja i jak ukus - blago je sladak i ostavlja hladan osećaj.",
                454, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(11, "Tegla meda, suncokret", 1100,
                "Suncokretov med odlikuje zlatna boja, kao i bogat i kremast ukus. Predstavlja ukusan zaslađivač.",
                454, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(12, "Medni medved, kupina", 900,
                "Med od kupine odlikuje jedinstven voćni ukus i on predstavlja odličan zaslađivač, ali i sjajan izbor za sve namene.",
                340, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(13, "Medni medved, detelina", 800,
                "Med od deteline ima klasičan, zaokružen i poznat ukus koji je podložan širokom spektru kulinarskih namena.",
                340, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));
        products.add(new Product(14, "Medni medved, divlje cveće", 900,
                "Ovaj med ima taman, topao i bogat ukus koji se menja sa godišnjim dobom. Omiljeni je med profesionalnih pekara.",
                340, "Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu."));

        for (User u: users) {
            if (u.getType() == false){
                baskets.add(new Basket(u.getId()));
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void login(View view) {
        String username = ((EditText) findViewById(R.id.username)).getText().toString();
        String password = ((EditText) findViewById(R.id.password)).getText().toString();

        for (User u: users){
            if ((u.getUsername().equals(username)) && (u.getPassword().equals(password))){
                if (u.getType() == false){
                    Intent intent = new Intent(this, CustomerActivity.class);
                    intent.putExtra("userId", u.getId());
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Ova aplikacija je namenjena samo kupcima. Molimo Vas da koristite web aplikaciju.", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
        Toast.makeText(this, "Neispravni podaci.", Toast.LENGTH_SHORT).show();

    }
}