const products = [
    {
        id: 1,
        name: 'Tegla meda, kupina',
        price: 1300,
        description: 'Med od kupine odlikuje jedinstven voćni ukus i on predstavlja odličan zaslađivač, ali i sjajan izbor za sve namene.',
        weight: 454,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 2,
        name: 'Tegla meda, detelina',
        price: 1100,
        description: 'Med od deteline ima klasičan, zaokružen i poznat ukus koji je podložan širokom spektru kulinarskih namena.',
        weight: 454,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 3,
        name: 'Tegla meda, malina',
        price: 1300,
        description: 'Med od maline dolazi sa jedne male porodične farme. Svetle je boje, a ima blag i nežan ukus.',
        weight: 454,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 4,
        name: 'Tegla meda, pomorandžin cvet',
        price: 1300,
        description: 'Med od pomorandžinog cveta je svetle boje i laganog ukusa sa akcentom na citrus koji odražava delikatnost ovih cvetova.',
        weight: 454,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 5,
        name: 'Tegla meda, divlje cveće',
        price: 1300,
        description: 'Ovaj med ima taman, topao i bogat ukus koji se menja sa godišnjim dobom. Omiljeni je med profesionalnih pekara.',
        weight: 454,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 6,
        name: 'Tegla meda, prolećni nektar',
        price: 1100,
        description: 'Prolećni nektar je med koji podseća na blago začinjene prolećne cvetove i predstavlja sjajan dodatak jelima.',
        weight: 454,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 7,
        name: 'Tegla meda, heljda',
        price: 1100,
        description: 'Heljdin med je izrazito tamne boje i ima pun, bogat ukus. Tamniji medovi obično imaju veći sadržaj minerala nego svetliji.',
        weight: 454,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 8,
        name: 'Tegla meda, čičak',
        price: 1300,
        description: 'Ovaj med je lagane boje i ukusa, ali i odiše svežinom. Koristite ga kao zaslađivač u omiljenom receptu.',
        weight: 454,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 9,
        name: 'Tegla meda, livadska pena',
        price: 1100,
        description: 'Ovaj med ima lagan ukus koji podseća na vanilu. Veoma je popularan kod lokalnih ljubitelja meda.',
        weight: 454,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 10,
        name: 'Tegla meda, borovnica',
        price: 1300,
        description: 'Med  od borovnice ima lagan ukus sa suptilnim notama prepečenog belog sleza i vanile čineći ukusan dodatak jelima.',
        weight: 454,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 11,
        name: 'Tegla meda, nana',
        price: 1300,
        description: 'Med od nane odlikuje zlatna boja i jak ukus - blago je sladak i ostavlja hladan osećaj.',
        weight: 454,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 12,
        name: 'Tegla meda, suncokret',
        price: 1100,
        description: 'Suncokretov med odlikuje zlatna boja, kao i bogat i kremast ukus. Predstavlja ukusan zaslađivač.',
        weight: 454,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 13,
        name: 'Medni medved, kupina',
        price: 900,
        description: 'Med od kupine odlikuje jedinstven voćni ukus i on predstavlja odličan zaslađivač, ali i sjajan izbor za sve namene.',
        weight: 340,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 14,
        name: 'Medni medved, detelina',
        price: 800,
        description: 'Med od deteline ima klasičan, zaokružen i poznat ukus koji je podložan širokom spektru kulinarskih namena.',
        weight: 340,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    },
    {
        id: 15,
        name: 'Medni medved, divlje cveće',
        price: 900,
        description: 'Ovaj med ima taman, topao i bogat ukus koji se menja sa godišnjim dobom. Omiljeni je med profesionalnih pekara.',
        weight: 340,
        usage: 'Rok trajanja je dve godine. Čuvajte na hladnom i mračnom mestu.'
    }

]

export default products;