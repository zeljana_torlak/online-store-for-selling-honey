const users = [
    {
        id: 0,
        username: 'marko',
        password: '123',
        name: 'Marko',
        surname: 'Marković',
        phoneNumber: '0645676543',
        address: 'Njegoševa 15',
        type: 0
    },
    {
        id: 1,
        username: 'jelena',
        password: '123',
        name: 'Jelena',
        surname: 'Jelić',
        phoneNumber: '0643434567',
        address: 'Krunska 5',
        type: 1
    },
    {
        id: 2,
        username: 'ivana',
        password: '123',
        name: 'Ivana',
        surname: 'Ivanović',
        phoneNumber: '0645453521',
        address: 'Vojislava Ilića 24',
        type: 0
    }
]

export default users;