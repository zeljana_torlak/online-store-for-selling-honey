const baskets = [
    {
        username: 'marko',
        items: [
            {
                productId: 7,
                quantity: 1
            },
            {
                productId: 13,
                quantity: 1
            }
        ]
    }
]

export default baskets;