const orders = [
    {
        id: 1,
        username: 'marko',
        date: '22.11.2020. 11:30',
        price: 3500,
        items: [
            {
                productId: 3,
                quantity: 2
            },
            {
                productId: 15,
                quantity: 1
            }
        ],
        status: 0
    }
]

export default orders;