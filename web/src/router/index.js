import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from "@/views/Login";
import Customer from "@/views/Customer";
import Salesman from "@/views/Salesman";
import ProductDetails from "@/views/ProductDetails";
import Products from "@/views/Products";
import Basket from "@/views/Basket";
import SalesmanProfile from "@/views/SalesmanProfile";
import SalesmanChangePassword from "@/views/SalesmanChangePassword";
import SalesmanChangeProfile from "@/views/SalesmanChangeProfile";
import CustomerProfile from "@/views/CustomerProfile";
import CustomerChangeProfile from "@/views/CustomerChangeProfile";
import CustomerChangePassword from "@/views/CustomerChangePassword";
import Orders from "@/views/Orders";
import OrderDetails from "@/views/OrderDetails";
import NewProduct from "@/views/NewProduct";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Login',
        component: Login
    },
    {
        path: '/kupac',
        name: 'Customer',
        component: Customer
    },
    {
        path: '/kupac/profil',
        name: 'CustomerProfile',
        component: CustomerProfile
    },
    {
        path: '/kupac/izmenapodataka',
        name: 'ChangeProfile',
        component: CustomerChangeProfile
    },
    {
        path: '/kupac/izmenalozinke',
        name: 'ChangePassword',
        component: CustomerChangePassword
    },
    {
        path: '/kupac/proizvodi',
        name: 'Products',
        component: Products
    },
    {
        path: '/kupac/proizvod/:id',
        name: 'ProductDetails',
        component: ProductDetails
    },
    {
        path: '/kupac/korpa',
        name: 'Basket',
        component: Basket
    },
    {
        path: '/prodavac',
        name: 'Salesman',
        component: Salesman
    },
    {
        path: '/prodavac/profil',
        name: 'SalesmanProfile',
        component: SalesmanProfile
    },
    {
        path: '/prodavac/izmenapodataka',
        name: 'SalesmanChangeProfile',
        component: SalesmanChangeProfile
    },
    {
        path: '/prodavac/izmenalozinke',
        name: 'SalesmanChangePassword',
        component: SalesmanChangePassword
    },
    {
        path: '/prodavac/narudzbine',
        name: 'Orders',
        component: Orders
    },
    {
        path: '/prodavac/narudzbina/:id',
        name: 'OrderDetails',
        component: OrderDetails
    },
    {
        path: '/prodavac/noviproizvod',
        name: 'NewProduct',
        component: NewProduct
    }
    /*{
        path: '/about',
        name: 'About',
        component: () => import('../views/About.vue')
    }*/
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
